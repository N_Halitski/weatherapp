//
//  WeatherRealm.swift
//  Weather
//
//  Created by Nikita on 22.10.2021.
//

import Foundation
import RealmSwift

class RealmWeather: Object {
    @Persisted var dateAndTime: String = ""
    @Persisted var address: String = ""
    @Persisted var coordinates: RealmCoordinate?
    @Persisted var tempInfo: RealmTemperatureInfo?
    @Persisted var weatherInfo: RealmWeatherData?
    @Persisted var cityInfo: RealmCityName?
}

class RealmCoordinate: Object{
    @Persisted var longitude: Double = 0
    @Persisted var latitude: Double = 0
}

class RealmWeatherData: Object {
    @Persisted var desc: String = ""
    @Persisted var icon: String = ""
}

class RealmTemperatureInfo: Object{
    @Persisted var humidity: Int = 0
    @Persisted var pressure: Int = 0
    @Persisted var temperature: Double = 0
}

class RealmCityName: Object {
    @Persisted var cityName: String = ""
}

