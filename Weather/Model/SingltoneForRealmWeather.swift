//
//  SingleToneForRealmWeather.swift
//  Weather
//
//  Created by Nikita on 25.10.2021.
//

import Foundation

class SingletonForRealm {
    
    static var shared = SingletonForRealm()
    
    var weatherData: CurrentWeather!
}
