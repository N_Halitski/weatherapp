

import Foundation
import Alamofire

class ServerManager{
    let apiKey: String = "a0f5dc5924cba1b5e385de0438393819"
    var baseUrl: String = "http://api.openweathermap.org"
    
    
    static let shared = ServerManager()
    
    func downloadCurrentWeather(_ latitude: Double,_ longitude: Double, onCompletion:@escaping ((CurrentWeather))->Void){
        let apiKey = "a0f5dc5924cba1b5e385de0438393819"
        let baseUrl = "https://api.openweathermap.org"
        
        var param: [String: Any] = [:]
        let latitude = latitude
        let longitude = longitude
        
        param["lat"] = latitude
        param["lon"] = longitude
        param["appid"] = apiKey
        param["units"] = "metric"
        let url = "\(baseUrl)/data/2.5/weather?lat=\(latitude)&lon=\(longitude)&units=metric&api=\(apiKey)"
        
        
        AF.request(url, method: .get, parameters: param).responseJSON { response in
            
            var currentWeather = CurrentWeather()
            
            if let responseDictionary = response.value as? [String:Any] {
               
                // MARK: создал кармашки для погоды и заполняю их
                currentWeather.coordinates = Coordinate()
                currentWeather.tempInfo = TemperatureInfo()
                currentWeather.cityInfo = CityName()
                currentWeather.weatherInfo = Weather()
                
                if let coordDict = responseDictionary["coord"] as? [String:Any] {
                    if let latitude = coordDict["lat"] as? Double,
                       let longitude = coordDict["lon"] as? Double{
                        
                        var coordinates = Coordinate()
                        coordinates.latitude = latitude
                        coordinates.longitude = longitude
                        
                        currentWeather.coordinates = coordinates
                    }
                }
                if let tempInfo = responseDictionary["main"] as? [String: Any] {
                    if let humidity = tempInfo["humidity"] as? Int,
                       let pressure = tempInfo["pressure"] as? Int,
                       let temperature = tempInfo["temp"] as? Double,
                       let tempMax = tempInfo["temp_max"] as? Double,
                       let tempMin = tempInfo["temp_min"] as? Double{
                        
                        var tempInfo = TemperatureInfo()
                        tempInfo.humidity = humidity
                        tempInfo.pressure = pressure
                        tempInfo.temperature = temperature
                        tempInfo.tempMax = tempMax
                        tempInfo.tempMin = tempMin
                        
                        currentWeather.tempInfo = tempInfo
                    }
                }
                if let cityName = responseDictionary["name"] as? String{
                    
                    var cityInfo = CityName()
                    cityInfo.cityName = cityName
                    
                    currentWeather.cityInfo = cityInfo
                    
                }
                if let weatherInfo = responseDictionary["weather"] as? [[String:Any]]{
                    for dataForWeather in weatherInfo{
                        if let description = dataForWeather["description"] as? String,
                           let weatherIcon = dataForWeather["icon"] as? String{
                            
                            var weatherInfo = Weather()
                            weatherInfo.desc = description
                            weatherInfo.icon = weatherIcon
                            
                            currentWeather.weatherInfo = weatherInfo
                        }
                    }
                }
                
            }
            onCompletion(currentWeather)
        }
    }
    
}
