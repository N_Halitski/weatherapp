//
//  CurrentWeather.swift
//  Weather
//
//  Created by Nikita on 20.10.2021.
//

import Foundation

struct CurrentWeather {
    var coordinates: Coordinate?
    var tempInfo: TemperatureInfo?
    var weatherInfo: Weather?
    var cityInfo: CityName?
    var dateAndTime: String?
    var address: String?
    
}

struct Coordinate {
    var longitude: Double?
    var latitude: Double?
}

struct TemperatureInfo {
    var humidity: Int?
    var pressure: Int?
    var temperature: Double?
    var tempMax: Double?
    var tempMin: Double?
}

struct Weather {
    var desc: String?
    var icon: String?
}

struct CityName {
    var cityName: String?
}


