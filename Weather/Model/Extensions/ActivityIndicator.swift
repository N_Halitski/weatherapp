//
//  ActivityIndicator.swift
//  Weather
//
//  Created by Nikita on 22.10.2021.
//

import UIKit
import NVActivityIndicatorView

extension MainViewController{
    
    func setUpActivityIndicator() {
        let indicatorsSize: CGFloat = 70
        let indicatorsFrame = CGRect(x: (view.frame.width - indicatorsSize)/2 , y: (view.frame.height - indicatorsSize)/2, width: indicatorsSize, height: indicatorsSize)
        activityIndicator = NVActivityIndicatorView(frame: indicatorsFrame, type: .ballPulse, color: .white, padding: 20)
        view.addSubview(activityIndicator)
    }
}

