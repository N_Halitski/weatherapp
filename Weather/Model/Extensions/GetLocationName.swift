//
//  GetLocationName.swift
//  Weather
//
//  Created by Nikita on 24.10.2021.
//

import Foundation
import CoreLocation


extension MainViewController{
    func getLocationName(with location: CLLocation, completion: @escaping ((String?)-> Void)) {
        
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(location, preferredLocale: .current) { placemarks, error in
            guard let place = placemarks?.first, error == nil else { completion(nil); return }
            
            var name = ""
            
            if let areaOfInterest = place.areasOfInterest{
                for data in areaOfInterest{
                    name += " \(data) ,"
                }
            }
            
            if let locality = place.locality {
                name += " \(locality)"
            }
            
            if let administrativeRegion = place.administrativeArea {
                name += ", \(administrativeRegion)"
            }
            completion(name)
        }
    }
}
