//
//  DateFormater.swift
//  Weather
//
//  Created by Nikita on 22.10.2021.
//

import Foundation

extension MainViewController {
    
    func getWeatherDate() -> String {
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT")
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "MM-dd-yyyy HH:mm"
        let strDate = dateFormatter.string(from: date)
        return strDate
    }
    
}
