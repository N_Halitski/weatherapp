//
//  WeatherIconExtension.swift
//  Weather
//
//  Created by Nikita on 20.10.2021.
//

import UIKit

    func pickImageForWeather(_ imageId: String) -> UIImage{
        
        var image = UIImage()
        
        if imageId == "01d" || imageId == "01n"{
            image = UIImage(named: "01d")!
        } else if imageId == "02d" || imageId == "02n"{
            image = UIImage(named: "02d")!
        } else if imageId == "03d" || imageId == "03n"{
            image = UIImage(named: "03d")!
        }else if imageId == "04d" || imageId == "04n"{
            image = UIImage(named: "04d")!
        }else if imageId == "09d" || imageId == "09n"{
            image = UIImage(named: "09d")!
        }else if imageId == "10d" || imageId == "10n"{
            image = UIImage(named: "10d")!
        }else if imageId == "11d" || imageId == "11n"{
            image = UIImage(named: "11d")!
        }else if imageId == "13d" || imageId == "13n"{
            image = UIImage(named: "13d")!
        }else if imageId == "50d" || imageId == "50n"{
            image = UIImage(named: "50d")!
        }
        
        return image
    }



