//
//  WeatherListViewController.swift
//  Weather
//
//  Created by Nikita on 22.10.2021.
//

import UIKit
import RealmSwift

class WeatherListViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var weatherList: Results<RealmWeather>!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "History"
        
        tableView.delegate = self
        tableView.dataSource = self
        
        let singleWeatherCell = UINib(nibName: "SingleWeatherCell", bundle: nil)
        tableView.register(singleWeatherCell, forCellReuseIdentifier: "SingleWeatherCell")
        
        let realm = try! Realm()
        weatherList = realm.objects(RealmWeather.self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if var textAttributes = navigationController?.navigationBar.titleTextAttributes {
            textAttributes[NSAttributedString.Key.foregroundColor] = self.tableView.backgroundColor
            navigationController?.navigationBar.titleTextAttributes = textAttributes
        }
        tableView.reloadData()
    }
    
}
extension WeatherListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if weatherList.count == 0{
            self.tableView.setEmptyMessage("You haven't retrieve any weather calls yet")
        } else {
            self.tableView.restore()
        }
        
        return weatherList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SingleWeatherCell") as! SingleWeatherCell
        cell.selectionStyle = .none
        let weatherHistory = weatherList[indexPath.row]
        
        guard let coordinates = weatherHistory.coordinates else { return cell }
        cell.cityName.text = weatherHistory.address
        cell.coordinates.text =  "Coordinates: latitude:\(coordinates.latitude), longitude:\(coordinates.longitude)"
        cell.dateInfo.text = weatherHistory.dateAndTime
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let weatherDataVC = WeatherDataViewController()
        let weatherHistory = weatherList[indexPath.row]
        
        weatherDataVC.weather = weatherHistory
        self.navigationController?.pushViewController(weatherDataVC, animated: true)
    }
    
}
