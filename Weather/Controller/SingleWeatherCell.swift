//
//  SingleWeatherCell.swift
//  Weather
//
//  Created by Nikita on 22.10.2021.
//

import UIKit

class SingleWeatherCell: UITableViewCell {

    @IBOutlet weak var cityName: UILabel!
    @IBOutlet weak var coordinates: UILabel!
    @IBOutlet weak var dateInfo: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

   
    
}
