//
//  ViewController.swift
//  Weather
//
//  Created by Nikita on 20.10.2021.
//

import UIKit
import RealmSwift
import CoreLocation
import NVActivityIndicatorView

class MainViewController: UIViewController, CLLocationManagerDelegate {
    
    @IBOutlet weak var weatherIcon: UIImageView!
    @IBOutlet weak var temperature: UILabel!
    @IBOutlet weak var cityName: UILabel!
    @IBOutlet weak var coordinatesLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    
    let locationManager = CLLocationManager()
    let realm = try! Realm()
    var activityIndicator: NVActivityIndicatorView!
    var date: String = ""
    var expleciteLocationData: String = ""
    var callCompleted: Bool = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpActivityIndicator()
        activityIndicator.startAnimating()
        
        locationManager.delegate = self
        locationManager.requestLocation()
        locationManager.requestWhenInUseAuthorization()
        
        print(Realm.Configuration.defaultConfiguration.fileURL?.path)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first{
            let latitude = location.coordinate.latitude
            let longitude = location.coordinate.longitude
            
            date = self.getWeatherDate()
            
            //MARK: getting explicite location name
            getLocationName(with: location) { [weak self] locationName in
                guard let self = self, let locationName = locationName else {return}
                
                self.expleciteLocationData = locationName
                
            }
            
            ServerManager.shared.downloadCurrentWeather(latitude, longitude) { [weak self] currentWeather in
                guard let self = self else {return}
                
                self.activityIndicator.stopAnimating()
                
                if let temperature = currentWeather.tempInfo?.temperature,
                   let cityName = currentWeather.cityInfo?.cityName,
                   let latitude = currentWeather.coordinates?.latitude,
                   let longitude = currentWeather.coordinates?.longitude,
                   let image = currentWeather.weatherInfo?.icon{
                    self.temperature.text = String(format:"%.0f", temperature) + " " + "°C"
                    self.cityName.text = cityName
                    self.coordinatesLabel.text = "Coordinates: \(latitude), \(longitude)"
                    let iconForWeather = pickImageForWeather(image)
                    self.weatherIcon.image = iconForWeather
                    self.addressLabel.text = "Address: \(self.expleciteLocationData)"
                    
                }
                
                if !self.callCompleted{
                    //MARK: Saving data to realm
                    let realmWeather = RealmWeather()

                    if let cityName = currentWeather.cityInfo?.cityName,
                       let temperature = currentWeather.tempInfo?.temperature,
                       let latitude = currentWeather.coordinates?.latitude,
                       let longitude = currentWeather.coordinates?.longitude,
                       let icon = currentWeather.weatherInfo?.icon,
                       let desc = currentWeather.weatherInfo?.desc,
                       let humidity = currentWeather.tempInfo?.humidity,
                       let pressure = currentWeather.tempInfo?.pressure{

                        realmWeather.dateAndTime = self.date
                        realmWeather.address = self.expleciteLocationData
                        realmWeather.cityInfo = RealmCityName()
                        realmWeather.cityInfo?.cityName = cityName
                        realmWeather.tempInfo = RealmTemperatureInfo()
                        realmWeather.tempInfo?.temperature = temperature
                        realmWeather.tempInfo?.humidity = humidity
                        realmWeather.tempInfo?.pressure = pressure
                        realmWeather.coordinates = RealmCoordinate()
                        realmWeather.coordinates?.latitude = latitude
                        realmWeather.coordinates?.longitude = longitude
                        realmWeather.weatherInfo = RealmWeatherData()
                        realmWeather.weatherInfo?.icon = icon
                        realmWeather.weatherInfo?.desc = desc
                       

                    }

                    try! self.realm.write{
                        self.realm.add(realmWeather)

                    }
                    self.callCompleted = true
                }
            }
        }
        callCompleted = false
    }
    
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        activityIndicator.stopAnimating()
        showAlertButtonTapped()
        print("Failed to get users location : \(error.localizedDescription).")
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        guard status == .authorizedWhenInUse else { return }
        manager.startUpdatingLocation()
    }
    
    @IBAction func showAlertButtonTapped() {
        
        // create the alert
        let alert = UIAlertController(title: "Ooops", message: "Failed to fetch the weather, restart the app", preferredStyle: UIAlertController.Style.alert)
        
        
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
}

