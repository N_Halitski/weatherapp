//
//  WeatherDataViewController.swift
//  Weather
//
//  Created by Nikita on 26.10.2021.
//

import UIKit
import RealmSwift

class WeatherDataViewController: UIViewController {
    
    @IBOutlet weak var wetherImage: UIImageView!
    @IBOutlet weak var cityNameLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var coordinatesLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var pressureLabel: UILabel!
    @IBOutlet weak var dateAndTimeLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    
    var weather: RealmWeather!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.tintColor = .black
        if var textAttributes = navigationController?.navigationBar.titleTextAttributes {
            textAttributes[NSAttributedString.Key.foregroundColor] = self.view.backgroundColor
            navigationController?.navigationBar.titleTextAttributes = textAttributes
        }
       
        if let icon = weather.weatherInfo?.icon,
           let cityName = weather.cityInfo?.cityName,
           let temperature = weather.tempInfo?.temperature,
           let coordinates = weather.coordinates,
           let desc = weather.weatherInfo?.desc,
           let humidity = weather.tempInfo?.humidity,
           let pressure = weather.tempInfo?.pressure{
            let weatherIcon = pickImageForWeather(icon)
            wetherImage.image = weatherIcon
            cityNameLabel.text = cityName
            temperatureLabel.text = String(format:"%.0f", temperature) + " " + "°C"
            coordinatesLabel.text = "Coordinates: \(coordinates.latitude) | \(coordinates.longitude)"
            descLabel.text = "Description: \(desc)"
            humidityLabel.text = "Humidity: \(String(humidity) + "%")"
            pressureLabel.text = "Pressure: \(String(pressure) + " " + "hPa")"
            dateAndTimeLabel.text = "Date and time: \(weather.dateAndTime)"
            addressLabel.text = "Location: \(weather.address)"
            
            title = cityName
        }
       
    }

    @IBAction func viewLocationClicked(_ sender: Any) {
        if let coordinates = weather.coordinates{
            let mapVC = MapViewController()
            
            mapVC.latitude = coordinates.latitude
            mapVC.longitude = coordinates.longitude
            
            navigationController?.pushViewController(mapVC, animated: true)
        }
        
        
    }
    
}
