//
//  MapViewController.swift
//  Weather
//
//  Created by Nikita on 26.10.2021.
//

import UIKit
import MapKit

class MapViewController: UIViewController {
    
    @IBOutlet weak var mapView: MKMapView!
    
    var longitude: Double?
    var latitude: Double?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let latitude = latitude, let longitude = longitude else { return }
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = .init(latitude: latitude, longitude: longitude)
        mapView.setRegion(MKCoordinateRegion(center: annotation.coordinate, span: MKCoordinateSpan(latitudeDelta: 0.7, longitudeDelta: 0.7)), animated: true)
        mapView.addAnnotation(annotation)
        
    }
    
}

